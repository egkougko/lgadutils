#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedtypedef;
#pragma link off all typedef;
#pragma link C++ nestedclass;
#pragma link C++ class vector<vector<pair<double, double>>>+;
#pragma link C++ enum AqInstrument;
#pragma link C++ enum AqBoard;
#pragma link C++ enum SecStage;
#pragma link C++ struct TrCrHist;
#pragma link C++ class LGADBase;
#pragma link C++ class WaveForm;
#pragma link C++ class LGADSel;
#pragma link C++ class DUTChannel;
#pragma link C++ class LGADRun;
#pragma link C++ class LGADUtils;

#endif

#ifdef __ROOTCLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedtypedef;
#pragma link off all typedef;
#pragma link C++ nestedclass;
#pragma link C++ class vector<vector<pair<double, double>>>+;
#pragma link C++ enum AqInstrument;
#pragma link C++ enum AqBoard;
#pragma link C++ enum SecStage;
#pragma link C++ struct TrCrHist;
#pragma link C++ class LGADBase;
#pragma link C++ class WaveForm;
#pragma link C++ class LGADSel;
#pragma link C++ class DUTChannel;
#pragma link C++ class LGADRun;
#pragma link C++ class LGADUtils;

#endif